// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyB90YEzRIbfDf3800jwyda1RsgFkVFiF0M",
    authDomain: "oshop-a3e4c.firebaseapp.com",
    databaseURL: "https://oshop-a3e4c.firebaseio.com",
    projectId: "oshop-a3e4c",
    storageBucket: "",
    messagingSenderId: "183236573443"
  }
};
